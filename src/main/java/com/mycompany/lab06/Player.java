package com.mycompany.lab06;

import java.io.Serializable;

public class Player{
    
    private String CurrentPlayer;
    
    private int winX;
    private int winO;
    private int drawing;
    
    public void setDefault(){
        this.CurrentPlayer = " X";
    }
    public void switchPlayer(String player){
        if(player.equals(" X")){
            this.CurrentPlayer = " O";
        }else{
            this.CurrentPlayer = " X";
        }
    }
    
    public String getCurrentplayer() {
        return CurrentPlayer;
    }

    public void setWinX() {
        this.winX++;
    }

    public void setWinO() {
        this.winO++;
    }

    public void setDrawing() {
        this.drawing++;
    }

    public int getWinX() {
        return winX;
    }

    public int getWinO() {
        return winO;
    }

    public int getDrawing() {
        return drawing;
    }
    
}
