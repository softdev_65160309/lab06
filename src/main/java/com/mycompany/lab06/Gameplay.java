package com.mycompany.lab06 ;

import java.util.Scanner;

public class Gameplay {

    public static void printWelcome() {
        System.out.println("Welcome To XO Game!!");
    }

    public static boolean checkResult(boolean Win, boolean Draw, String player) {
        if (Win) {
            System.out.println(player + " is Winner!!!!!!!!!!!!!");
            return true;
        } else if (Draw) {
            System.out.println("This Game is Drawing!!!!!!!!!!!!!");
            return true;
        }
        return false;
    }

    public static boolean playAgain(String ans) {
        if (ans.equals("y")) {
            return true;
        }
        return false;
    }

    public static void showTurn(String player) {
        System.out.println(player + " Turn");
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (true) {
            printWelcome();
            Table table = new Table();
            Player player = new Player();

            while (true) {
                // System.out.println(table.getTable());
                showTurn(player.getCurrentplayer());
                while (true) {
                    int row = input.nextInt();
                    int col = input.nextInt();// set row col to table[][]
                    if (table.setRoCo(row, col, player.getCurrentplayer())) {
                        break;
                    }
                }
                table.showTable();// Show table
                if (checkResult(table.checkWin(player.getCurrentplayer()), table.checkDraw(), player.getCurrentplayer())) {
                    break; // if this game have winner or drawing it'll stop set row col loop then go to
                           // choose play again
                }
                player.switchPlayer(player.getCurrentplayer());

            }
            System.out.println("Play again? || y = Yes  : n = No");
            while (true) {
                String ans = input.next(); // choose play again or stop
                
                if(ans.equals("y")||ans.equals("n")){
                    if (playAgain(ans)) {
                        break;
                    } else {
                        System.out.println("Thank for playing");
                        return; // stop game
                    }
                    
                }
                
            }

        }

    }

}
