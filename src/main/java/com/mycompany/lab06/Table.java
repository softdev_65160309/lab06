package com.mycompany.lab06;

public class Table {

    private String[][] table = { { " -", " -", " -" }, { " -", " -", " -" }, { " -", " -", " -" } };

    Table(){
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(table[i][j]);
                    
                }
                System.out.println();
            }
    }

    public void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j]);

            }
            System.out.println();
        }
        System.out.println("________________________");
        
    }
    
    public void setNewTable(){
        for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    table[i][j] = " -";
                }
            }
    }
    
        public String getTable(int i, int j) {
                return table[i][j];
            }
        
    

    public boolean setRoCo(int row, int col, String gamePlayer) {
            if(row>2||col>2||row<0||col<0){
                System.out.println("Set position again");
                return false;
            }else if(table[row][col] == " -"){
                table[row][col] = gamePlayer;
                return true;
            }
            System.out.println("Set position again");
            return false;
        }
    

    public boolean checkWin(String player) {
        for (int i = 0; i < 3; i++) {
            if (table[0][i].equals(player) && table[1][i].equals(player) && table[2][i].equals(player)) {
                return true;
            } else if (table[i][0].equals(player) && table[i][1].equals(player) && table[i][2].equals(player)) {
                return true;
            }
        }
        if (table[0][0].equals(player) && table[1][1].equals(player) && table[2][2].equals(player)) {
            return true;
        } else if (table[0][2].equals(player) && table[1][1].equals(player) && table[2][0].equals(player)) {
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        int round = 0;
        for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if(table[i][j].equals(" X") || table[i][j].equals(" O")){
                        round++;
                        if(round == 9){
                            return true;
                        }
                    }

                }
            }
        return false;
    }

}
