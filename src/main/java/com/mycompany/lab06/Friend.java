/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab06;

import java.io.Serializable;

/**
 *
 * @author Anony
 */
class Friend implements Serializable{
    private String name;
    private int age;
    private String tel;
    private int id;
    private static int lastId=1;
    
    public Friend(String name , int age , String tel){
        this.id = lastId++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) throws Exception {
        if(age<=0){
            throw new Exception();
        }
        this.age = age;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getTel() {
        return tel;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", age=" + age + ", tel=" + tel + ", id=" + id + '}';
    }
    
    
}
